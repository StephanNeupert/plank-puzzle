// Size  4x4
// Score: 22
// Plank Moves: 10
SIZ 6 4
STA 0 1
END 5 0
SOL 0   0 0 1 1 0 1
SOL 1   1 1 0 0 1 0
SOL 2   0 0 0 0 0 0
SOL 3   0 1 1 1 1 0
PLA 0 1 E 1
PLA 1 1 S 2
PLA 3 0 S 3
// Solution
// 0 1 3E-0 2 3E-2 2 3N-0 2 3W-2 1 1E-0 1 1W-1 4 1S-0 4 3W-2 3 3N-1 3 0E-
