﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;


namespace PlankCreator
{
    partial class FormPlankCreator : BufferedForm
    {
        public FormPlankCreator()
        {
            InitializeComponent();
            AddPlankBoxes();
            Utility.InitializeRandom();
        }

        private Level level;
        private List<TextBox> txtPlanks;

        /// <summary>
        /// Adds 8 text boxes to enter plank lengths.
        /// </summary>
        private void AddPlankBoxes()
        {
            txtPlanks = new List<TextBox>();
            TextBox txtPlank;
            for (int i = 0; i < 8; i++)
            {
                txtPlank = new TextBox();
                txtPlank.Top = 131 + (i / 4) * 26;
                txtPlank.Left = 10 + (i % 4) * 74;
                txtPlank.Height = 20;
                txtPlank.Width = 68;
                txtPlank.TextAlign = HorizontalAlignment.Center;
                if (i < 3) txtPlank.Text = (i + 1).ToString();

                txtPlanks.Add(txtPlank);
                this.Controls.Add(txtPlank);
            }
        }

        /// <summary>
        /// Solves a user-specified level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butSolve_Click(object sender, EventArgs e)
        {
            string filter = "Plank Puzzle levels|*.txt";
            string filePath = Utility.SelectFile(filter);
            if (string.IsNullOrWhiteSpace(filePath)) return;

            // Setting lblOutput.Text
            lblOutput.Text = "Working...";
            lblOutput.Update();

            // Start measuring time for execution
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            
            // Test level
            level = LevelLoader.LoadFromFile(filePath);
            Solver solver = new Solver(level);
            List<Move> moves = solver.SolveLevel();            
            WriteOutput.SaveSolution(moves);
            
            stopwatch.Stop();

            // Setting lblOutput.Text
            string result = (moves?.Count > 0) ? "Done! " + moves.Count.ToString() + "moves, score " + moves.Score().ToString() + ". " : "Done! Level unsolvable! "; 
            if (stopwatch.Elapsed.TotalMilliseconds < 5000)
            {
                lblOutput.Text = result + "Elapsed time: " + stopwatch.Elapsed.TotalMilliseconds.ToString() + "ms";
            }
            else
            {
                lblOutput.Text = result + "Elapsed time: " + stopwatch.Elapsed.TotalSeconds.ToString() + "s";
            }
        }

        /// <summary>
        /// Creates a completely new level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butCreateNew_Click(object sender, EventArgs e)
        {
            // Setting lblOutput.Text
            lblOutput.Text = "Working...";
            lblOutput.Update();

            // Start measuring time for execution
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            int width = 1;
            int height = 1;
            int fillRatio = 1;
            int numTries = 100;
            List<int> plankLengths = new List<int>();
            try
            {
                width = int.Parse(txtWidth.Text);
                height = int.Parse(txtHeight.Text);
                fillRatio = int.Parse(txtFillRatio.Text) % 100;
                numTries = int.Parse(txtNumTries.Text);

                // Read plank lengths
                for (int i = 0; i < int.Parse(txtPlankNum.Text); i++)
                {
                    plankLengths.Add(int.Parse(txtPlanks[i].Text));
                }
            }
            catch
            {
                lblOutput.Text = "Error: Could not read text fields.";
                stopwatch.Stop();
                return;
            }

            // Create a new level
            Creator creator = new Creator(width, height, plankLengths, fillRatio, numTries);
            PuzzleInfo puzzle = creator.CreatePuzzle(this, lblOutput);

            stopwatch.Stop();

            if (puzzle == null || puzzle.Score == -100)
            {
                lblOutput.Text = "Error: Could not create solvable puzzle.";
            }
            else
            {
                WriteOutput.SaveLevel(puzzle, "Level_New");
                if (stopwatch.Elapsed.TotalMilliseconds < 5000)
                {
                    lblOutput.Text = "Done! Elapsed time: " + stopwatch.Elapsed.TotalMilliseconds.ToString() + "ms";
                }
                else if (stopwatch.Elapsed.TotalSeconds < 120)
                {
                    lblOutput.Text = "Done! Elapsed time: " + stopwatch.Elapsed.TotalSeconds.ToString() + "s";
                }
                else
                {
                    lblOutput.Text = "Done! Elapsed time: " + ((int)stopwatch.Elapsed.TotalMinutes).ToString() + "min";
                }
            }
        }

        /// <summary>
        /// Optimizes a user-selected level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butModify_Click(object sender, EventArgs e)
        {
            string filter = "Plank Puzzle levels|*.txt";
            string filePath = Utility.SelectFile(filter);
            if (string.IsNullOrWhiteSpace(filePath)) return;

            // Setting lblOutput.Text
            lblOutput.Text = "Working...";
            lblOutput.Update();

            // Start measuring time for execution
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            // Load Level from file
            level = LevelLoader.LoadFromFile(filePath);

            int numTries = 0;
            try
            {
                numTries = int.Parse(txtNumTries.Text);
            }
            catch
            {
                numTries = 100;
            }

            // Try to solve the level
            Adapter adapter = new Adapter(level, numTries);
            PuzzleInfo newPuzzle = adapter.Modify(lblOutput);

            // Save solution string
            WriteOutput.SaveLevel(newPuzzle, "Level_Mod");

            stopwatch.Stop();

            // Setting lblOutput.Text
            string result = (newPuzzle.Moves?.Count > 0) 
                ? "Done! " + newPuzzle.Moves.Count().ToString() + "moves, score " + newPuzzle.Score.ToString() + ". " 
                : "Done! Level unsolvable! ";

            if (stopwatch.Elapsed.TotalMilliseconds < 5000)
            {
                lblOutput.Text = result + "Elapsed time: " + stopwatch.Elapsed.TotalMilliseconds.ToString() + "ms";
            }
            else
            {
                lblOutput.Text = result + "Elapsed time: " + stopwatch.Elapsed.TotalSeconds.ToString() + "s";
            }
        }
    }
}