﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace PlankCreator
{
    class Plank : IEquatable<Plank>
    {
        public Point Start;
        public Point End => Start.Clone().Move(Dir, Length);

        public C.DIR Dir;
        public int Length;
        public bool IsReachable;

        public Plank() { }

        public Plank Clone()
        {
            Plank plank = new Plank();
            plank.Start = Start.Clone();
            plank.Dir = Dir;
            plank.Length = Length;
            plank.IsReachable = IsReachable;
            return plank;
        }

        public Plank Reverse()
        {
            Start = this.End;
            Dir = Dir.Opposite();
            return this;
        }

        /// <summary>
        /// Returns whether two planks intersect in the middle.
        /// </summary>
        /// <param name="otherPlank"></param>
        /// <returns></returns>
        public bool Intersects(Plank otherPlank)
        {
            // First put them in the correct form
            Plank plank1; // vertical
            Plank plank2; // horizontal

            if (this.Dir == otherPlank.Dir || this.Dir == otherPlank.Dir.Opposite())
            {
                return false;
            }
            else if (this.Dir.In(C.DIR.N, C.DIR.S))
            {
                if (this.Dir == C.DIR.S) plank1 = this.Clone();
                else plank1 = this.Reverse();

                if (otherPlank.Dir == C.DIR.E) plank2 = otherPlank.Clone();
                else plank2 = otherPlank.Reverse();
            }
            else
            {
                if (this.Dir == C.DIR.E) plank2 = this.Clone();
                else plank2 = this.Reverse();

                if (otherPlank.Dir == C.DIR.S) plank1 = otherPlank.Clone();
                else plank1 = otherPlank.Reverse();
            }

            return (plank1.Start.Y < plank2.Start.Y
                 && plank2.Start.Y < plank1.End.Y
                 && plank2.Start.X < plank1.Start.X
                 && plank1.Start.X < plank2.End.X);
        }

        /// <summary>
        /// Returns whether two planks have any common point, even at their ends.
        /// </summary>
        /// <param name="otherPlank"></param>
        /// <returns></returns>
        public bool IntersectsStrongly(Plank otherPlank)
        {
            // get direction correctly
            if (this.Dir.In(C.DIR.N, C.DIR.W)) this.Reverse();
            if (otherPlank.Dir.In(C.DIR.N, C.DIR.W)) otherPlank.Reverse();

            if (this.Dir == C.DIR.E)
            {
                if (otherPlank.Dir == C.DIR.E)
                {
                    return (this.Start.Y == otherPlank.Start.Y
                        && this.Start.X < otherPlank.End.X
                        && otherPlank.Start.X < this.End.X);
                }
                else
                {
                    return (otherPlank.Start.Y <= this.Start.Y
                        && this.Start.Y <= otherPlank.End.Y
                        && this.Start.X <= otherPlank.Start.X
                        && otherPlank.Start.X <= this.End.X
                        && (!this.Start.Equals(otherPlank.Start))
                        && (!this.Start.Equals(otherPlank.End))
                        && (!this.End.Equals(otherPlank.Start))
                        && (!this.End.Equals(otherPlank.End)));
                }
            }
            else // if (this.Dir == C.DIR.S)
            {
                if (otherPlank.Dir == C.DIR.S)
                {
                    return (this.Start.X == otherPlank.Start.X
                        && this.Start.Y < otherPlank.End.Y
                        && otherPlank.Start.Y < this.End.Y);
                }
                else
                {
                    return (this.Start.Y <= otherPlank.Start.Y
                        && otherPlank.Start.Y <= this.End.Y
                        && otherPlank.Start.X <= this.Start.X
                        && this.Start.X <= otherPlank.End.X
                        && (!this.Start.Equals(otherPlank.Start))
                        && (!this.Start.Equals(otherPlank.End))
                        && (!this.End.Equals(otherPlank.Start))
                        && (!this.End.Equals(otherPlank.End)));
                }
            }
        }

        public bool Equals(Plank otherPlank)
        {
            return (otherPlank != null 
                 && this.Start.Equals(otherPlank.Start) 
                 && this.Dir == otherPlank.Dir 
                 && this.Length == otherPlank.Length);
        }

    }

    struct Move
    {
        public int PlankIndex;
        public Point NewPos;
        public Point PrevPos;
        public C.DIR Direction;
        public C.DIR PrevDirection;
        public int PlankLength;

        public Move(int index, Point pos, C.DIR dir, Point prevPos, C.DIR prevDir, int length)
        {
            this.PlankIndex = index;
            this.NewPos = pos.Clone();
            this.Direction = dir;
            this.PrevPos = prevPos.Clone();
            this.PrevDirection = prevDir;
            this.PlankLength = length;
        }
    }

    class Level
    {
        // Constructor
        public Level() { }

        public int Width;
        public int Height;

        //public int GridSize;

        public Point StartPos;
        public Point TargetPos;

        public List<Plank> StartPlanks;
        public List<Plank> Planks;
        public HashSet<Point> Solids; // list of all solid squares
        public List<Move> Moves;

        /// <summary>
        /// Loads a level from a PuzzleInfo created by the puzzle creator.
        /// </summary>
        /// <param name="puzzleInfo"></param>
        public void LoadFromPuzzleInfo(PuzzleInfo puzzleInfo)
        {
            Width = puzzleInfo.Width;
            Height = puzzleInfo.Height;
            StartPlanks = puzzleInfo.Planks;
            Planks = puzzleInfo.Planks.ConvertAll(p => p.Clone());
            Solids = puzzleInfo.GetSolidSet();
            StartPos = new Point(0, puzzleInfo.StartPosY);
            TargetPos = new Point(-1, -1); // won't need it here.
            Moves = new List<Move>(); // don't copy existing moves
        }


        /// <summary>
        /// Returns all currently reachable squares.
        /// </summary>
        /// <returns></returns>
        public HashSet<Point> GetReachableSolids()
        {
            HashSet<Point> reachSolids = new HashSet<Point>();

            // Add all start and end squares of reachable planks
            Planks.FindAll(pla => pla.IsReachable)
                  .ForEach(pla => { reachSolids.Add(pla.Start); reachSolids.Add(pla.End); });

            return reachSolids;
        }

        /// <summary>
        /// Sets a plank, if possible.
        /// </summary>
        /// <param name="plankIndex"></param>
        /// <param name="newPos"></param>
        /// <param name="dir"></param>
        /// <param name="doSaveMove"></param>
        /// <returns></returns>
        public bool SetPlank(Plank plank, Point newPos, C.DIR dir, bool doSaveMove = true)
        {
            // Save old position and direction, if plank cannot be moved.
            Point oldPos = plank.Start.Clone();
            C.DIR oldDir = plank.Dir;

            // Get new plank at the correct location 
            plank.Start = newPos;
            plank.Dir = dir;

            if (!doSaveMove || MaySetPlank(plank))
            {
                // Add this to MoveList
                if (doSaveMove) Moves.Add(new Move(Planks.IndexOf(plank), newPos, dir, oldPos, oldDir, plank.Length));
                GetReachablePlanks(plank);
                return true;
            }
            else
            {
                plank.Start = oldPos;
                plank.Dir = oldDir;
                return false;
            }
        }

        /// <summary>
        /// Checks whether a plank may be placed at its current location.
        /// </summary>
        /// <param name="plank"></param>
        /// <returns></returns>
        private bool MaySetPlank(Plank plank)
        {
            // Check for solids in the middle
            Point newPos = plank.Start.Clone();
            for (int i = 1; i < plank.Length; i++)
            {
                newPos = newPos.Move(plank.Dir);
                if (Solids.Contains(newPos)) return false;
            }

            // Check for solid at the end
            newPos = newPos.Move(plank.Dir);
            if (!Solids.Contains(newPos)) return false;

            // Check for intersection with other planks
            if (Planks.Exists(pla => pla.Intersects(plank))) return false;

            return true;
        }

        /// <summary>
        /// Sets reachability from the starting plank for all planks correctly.
        /// </summary>
        /// <param name="startPlank"></param>
        public void GetReachablePlanks(Plank startPlank)
        { 
            // Reset reachable state for planks
            Planks.ForEach(pla => pla.IsReachable = false);
            startPlank.IsReachable = true;
            
            // Set initial set of reachable squares
            HashSet<Point> ReachSq = new HashSet<Point>() { startPlank.Start, startPlank.End };
            bool hasAddedPlank;

            do
            {
                hasAddedPlank = false;

                foreach (Plank pla in Planks)
                {
                    if (!pla.IsReachable && (ReachSq.Contains(pla.Start) || ReachSq.Contains(pla.End)))
                    {
                        pla.IsReachable = true;
                        ReachSq.Add(pla.Start);
                        ReachSq.Add(pla.End);
                        hasAddedPlank = true;
                    }
                }
            } while (hasAddedPlank);
        }

        /// <summary>
        /// Undos the latest move.
        /// </summary>
        public void UndoMove()
        {
            if (Moves.Count == 0) return;

            Move LastMove = Moves[Moves.Count - 1];
            SetPlank(Planks[LastMove.PlankIndex], LastMove.PrevPos, LastMove.PrevDirection, false);
            Moves.RemoveAt(Moves.Count - 1);
        }
        
        /// <summary>
        /// Checks whether we have solved the level
        /// </summary>
        /// <returns></returns>
        public bool IsLevelSolved()
        {
            // No need to check starting locations of planks.
            return Planks.Exists(pla => pla.IsReachable && pla.End.Equals(TargetPos));
        }
    }
}
