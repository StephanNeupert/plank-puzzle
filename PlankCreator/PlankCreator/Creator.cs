﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

namespace PlankCreator
{
    class Creator : LevelModifier
    {
        public Creator(int width, int height, List<int> plankLengths, int fillRatio, int numTries)
        {
            this.startPuzzle = new PuzzleInfo(new List<Plank>(), new C.Solid[width + 2, height]);
            this.fillRatio = fillRatio;
            this.numTries = numTries;
            this.plankLengths = plankLengths;
        }

        private PuzzleInfo startPuzzle; 

        private int width => startPuzzle.Width;
        private int height => startPuzzle.Height;
        private List<int> plankLengths;
        private int fillRatio;
        private int numTries;

        /// <summary>
        /// Starts creating a new puzzle and returns the best.
        /// </summary>
        /// <param name="form"></param>
        /// <param name="lblOutput"></param>
        /// <returns></returns>
        public PuzzleInfo CreatePuzzle(FormPlankCreator form, Label lblOutput)
        {
            PuzzleInfo bestPuzzle = null;

            for (int i = 0; i < numTries; i++)
            {
                PuzzleInfo newPuzzle = CreateNew();

                // Check whether this is better than the previous one
                if (bestPuzzle == null || bestPuzzle.Score < newPuzzle.Score)
                {
                    bestPuzzle = newPuzzle;
                    if (bestPuzzle.Score > 30) WriteOutput.SaveLevel(bestPuzzle, "Level_New");
                }

                // Update progress
                lblOutput.Text = "Working... (" + (i + 1).ToString() + " of " + numTries.ToString() + " tests done)";

                Application.DoEvents();
            }

            return bestPuzzle;
        }

        /// <summary>
        /// Creates a single new puzzle.
        /// </summary>
        /// <returns></returns>
        private PuzzleInfo CreateNew()
        {
            if (!TryInitialize()) return null;

            // Save first version with TestResults
            PuzzleInfo curPuzzle = startPuzzle.Clone();
            PuzzleInfo bestPuzzle = startPuzzle.Clone();
            bestPuzzle.Moves = TestPuzzle(bestPuzzle);

            // At first modify if arbitrarily
            int modifyTries = 1000 * Math.Max(1, (width - 5) * (height - 5));
            for (int i = 0; i < modifyTries; i++)
            {
                ModifyRandomSolid(curPuzzle, fillRatio);
                var moves = TestPuzzle(curPuzzle);
                if (moves.Score() > bestPuzzle.Score) bestPuzzle = curPuzzle;
            }

            if (bestPuzzle.Score < width * height) return bestPuzzle;

            // Now modify it 200 times intelligently per improvement 
            for (int i = 0; i < 200; i++)
            {
                curPuzzle = bestPuzzle.Clone();
                ModifyIntelligent(curPuzzle);
                var moves = TestPuzzle(curPuzzle);

                // reset counter for intelligent modifications, if we have an improvement
                if (moves.Score() > bestPuzzle.Score) i = 0;
                if (moves.Score() >= bestPuzzle.Score)
                {
                    bestPuzzle = curPuzzle;
                    bestPuzzle.Moves = moves;
                }
            }

            return bestPuzzle;
        }

        /* --------------------------------------------------------------
         *                     Initializing methods
         * --------------------------------------------------------------*/

        /// <summary>
        /// Take 50 tries to initialize the level.
        /// </summary>
        /// <returns></returns>
        private bool TryInitialize()
        {
            bool success;

            int i = 0;
            do
            {
                i++;
                success = Initialize();
            } while (i < 50 && !success);

            return success;
        }

        /// <summary>
        /// Tries to initialize a level.
        /// </summary>
        /// <returns></returns>
        private bool Initialize()
        {
            // Reset PlankList
            startPuzzle.Planks.Clear();
            foreach (int length in plankLengths)
            {
                Plank plank = new Plank();
                plank.Length = length;
                plank.Start = new Point(0, 0);
                plank.Dir = C.DIR.N;
                startPuzzle.Planks.Add(plank);
            }

            if (!InitializePlanks(Utility.Random(height))) return false;
            InitializeSolids();
            return true;
        }

        /// <summary>
        /// Tries to set all planks.
        /// </summary>
        /// <returns></returns>
        private bool InitializePlanks(int startPosY)
        {
            // Set a plank at the start position
            Plank startPlank = startPuzzle.Planks[Utility.Random(startPuzzle.Planks.Count)];
            startPlank.Start.Y = startPosY;
            startPlank.Dir = C.DIR.E;
            startPlank.IsReachable = true;

            // Set all other planks
            foreach (Plank plank in startPuzzle.Planks) 
            {
                if (plank != startPlank)
                {
                    if (!TryPlacePlank(plank)) return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Make 50 tries to lace the plank 
        /// </summary>
        /// <param name="plank"></param>
        /// <returns></returns>
        private bool TryPlacePlank(Plank plank)
        {
            int numTries = 0;
            bool mayPlace = true;
            do
            {
                numTries++;
                plank.Start.X = Utility.Random(width - 2) + 1;
                plank.Start.Y = Utility.Random(height);
                plank.Dir = (C.DIR)(Utility.Random(3));

                // Check whether end position is valid
                mayPlace = plank.End.X > 0  && plank.End.X < width - 1
                        && plank.End.Y >= 0 && plank.End.Y < height;

                // Check whether plank intersects others
                mayPlace = mayPlace && !startPuzzle.Planks.Exists(pla => pla != plank && pla.IntersectsStrongly(plank));
            } while (numTries < 50 && !mayPlace);

            return (numTries < 50);
        }

        /// <summary>
        /// Initializes the solids.
        /// </summary>
        private bool InitializeSolids()
        {
            // Initialize SolidArray
            for (int x = 0; x < width - 1; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    startPuzzle.Solids[x, y] = C.Solid.Empty;
                }
            }
            for (int y = 0; y < height; y++)
            {
                startPuzzle.Solids[width - 1, y] = C.Solid.ForceSolid;
            }

            // Set end positions of planks as solid and the middle as empty
            int numSolids = startPuzzle.Planks.ConvertAll(p => p.Start)
                                              .Concat(startPuzzle.Planks.ConvertAll(p => p.End))
                                              .Distinct().Count();
            startPuzzle.Planks.ForEach(plank => AddForceSolid(startPuzzle, plank));

            // Make approx. Fillratio% of all squares solid
            int targetNumSolids = (width - 2) * height * fillRatio / 100;
            while (numSolids < targetNumSolids)
            {
                if (TryPlaceSolid()) numSolids++;
                else return false;
            }

            return true;
        }

        /// <summary>
        /// Make 50 tries to place a solid. 
        /// </summary>
        /// <returns></returns>
        private bool TryPlaceSolid()
        {
            int numTries = 0;
            bool mayPlace = true;
            do
            {
                numTries++;
                int NewPosX = Utility.Random(width - 2) + 1;
                int NewPosY = Utility.Random(height);

                if (startPuzzle.Solids[NewPosX, NewPosY] == C.Solid.Empty)
                {
                    startPuzzle.Solids[NewPosX, NewPosY] = C.Solid.Solid;
                }
            } while (numTries < 50 && !mayPlace);

            return (numTries < 50);
        }

    }
}
