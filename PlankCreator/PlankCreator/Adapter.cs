﻿using System;
using System.Collections.Generic;

namespace PlankCreator
{
    class Adapter : LevelModifier
    {
        public Adapter(Level level, int numTries)
        {
            levelInfo = new PuzzleInfo(level);
            levelInfo.Moves = TestPuzzle(levelInfo);
            foreach (Plank plank in levelInfo.Planks)
            {
                AddForceSolid(levelInfo, plank);
            }

            this.numTries = numTries; 
        }

        private PuzzleInfo levelInfo;
        private int numTries;

        /// <summary>
        /// Optimizes the level.
        /// </summary>
        /// <returns></returns>
        public PuzzleInfo Modify(System.Windows.Forms.Label lblOutput)
        {
            int numImprovements = 0;

            for (int i = 0; i < numTries; i++)
            {
                PuzzleInfo newPuzzleInfo = levelInfo.Clone();
                ModifyIntelligent(newPuzzleInfo);
                newPuzzleInfo.Moves = new List<Move>();
                var moves = TestPuzzle(newPuzzleInfo);

                // reset counter for intelligent modifications, if we have an improvement
                if (moves.Score() > levelInfo.Score)
                {
                    i = 0;
                    numImprovements++;
                    lblOutput.Text = "Working... (" + numImprovements.ToString() + " improvements)";
                    System.Windows.Forms.Application.DoEvents();
                }
                if (moves.Score() >= levelInfo.Score)
                {
                    levelInfo = newPuzzleInfo;
                    levelInfo.Moves = moves;
                }

                if ((i + 1) % 10 == 0)
                {
                    lblOutput.Text = "Working... (" + numImprovements.ToString() + " improvements, "
                                                    + (i + 1).ToString() + " tries )";
                    System.Windows.Forms.Application.DoEvents();
                }
            }

            Reduce();
            return levelInfo;
        }

        /// <summary>
        /// Removes all unnecessary solids.
        /// </summary>
        public void Reduce()
        {
            // Solve the original level once more
            levelInfo.Moves = TestPuzzle(levelInfo);

            bool hasDeleted = false;
            do
            {
                hasDeleted = false;
                var solids = levelInfo.GetSolidList(false);

                foreach (var solid in solids)
                { 
                    PuzzleInfo newLevel = levelInfo.Clone();
                    newLevel.Solids[solid.X, solid.Y] = C.Solid.Empty;
                    var moves = TestPuzzle(newLevel);

                    if (moves.Score() >= levelInfo.Score)
                    {
                        levelInfo = newLevel;
                        levelInfo.Moves = moves;
                        hasDeleted = true;
                    }
                }
            } while (hasDeleted);
        }

    }
}
