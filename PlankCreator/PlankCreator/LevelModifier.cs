﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace PlankCreator
{
    abstract class LevelModifier
    {
        /// <summary>
        /// Test a single puzzle with variable end positions.
        /// </summary>
        /// <param name="puzzInfo"></param>
        /// <returns></returns>
        protected List<Move> TestPuzzle(PuzzleInfo puzzInfo)
        {
            // Create level and turn the last row solid 
            Level level = new Level();
            level.LoadFromPuzzleInfo(puzzInfo);
            for (int y = 0; y < level.Height; y++)
            {
                level.Solids.Add(new Point(level.Width - 1, y));
            }

            // Get reachable planks
            Plank startPlank = level.Planks.FirstOrDefault(pla => pla.Start.Equals(level.StartPos) || pla.End.Equals(level.StartPos));
            if (startPlank == null) return null;
            level.GetReachablePlanks(startPlank);

            // Try solving the level
            Solver solver = new Solver(level);
            var moveList = solver.SolveLevelNoTarget();

            // Return the best scoring moves.
            var scoreList = moveList.ConvertAll(moves => moves.Score());
            return moveList[scoreList.IndexOf(scoreList.Max())];
        }

        /// <summary>
        /// Switches a random (non-forced) solid between solid and empty.
        /// </summary>
        /// <param name="levelInfo"></param>
        /// <param name="fillRatio"></param>
        protected void ModifyRandomSolid(PuzzleInfo levelInfo, int fillRatio)
        {
            int newPosX;
            int newPosY;
            do
            {
                // Choose new square
                newPosX = Utility.Random(levelInfo.Width - 2) + 1;
                newPosY = Utility.Random(levelInfo.Height);

                // Never change the plank positions
                if    (levelInfo.Solids[newPosX, newPosY].In(C.Solid.Empty, C.Solid.Solid)
                  && ((levelInfo.Solids[newPosX, newPosY] == C.Solid.Solid) ^ (Utility.Random(100) < fillRatio)))
                {
                    break;
                }
            } while (true);

            // Change the value of one square (interchange the values 0 and 1)
            if (levelInfo.Solids[newPosX, newPosY] == C.Solid.Solid)
            {
                levelInfo.Solids[newPosX, newPosY] = C.Solid.Empty;
            }
            else
            {
                levelInfo.Solids[newPosX, newPosY] = C.Solid.Solid;
            }
        }

        /// <summary>
        /// Modifies a level in a more diverse and controlled fashion.
        /// </summary>
        /// <param name="levelInfo"></param>
        protected void ModifyIntelligent(PuzzleInfo levelInfo)
        {
            int changeType = Utility.Random(6);

            switch (changeType)
            {
                case 0:  ModifyDelete(levelInfo); break;
                case 1:  ModifyAdd(levelInfo); break;
                case 2:  ModifyDelete(levelInfo); ModifyAdd(levelInfo); break;
                default: ModifyPlanks(levelInfo); break;
            }
        }

        /// <summary>
        /// Deletes a solid (which is not forced)
        /// </summary>
        /// <param name="levelInfo"></param>
        private void ModifyDelete(PuzzleInfo levelInfo)
        {
            int numTries = 0;
            int newPosX;
            int newPosY;
            do
            {
                numTries++;
                newPosX = Utility.Random(levelInfo.Width - 2) + 1;
                newPosY = Utility.Random(levelInfo.Height);
            } while (numTries < 100 && !(levelInfo.Solids[newPosX, newPosY] == C.Solid.Solid));

            // Change the value of one square
            levelInfo.Solids[newPosX, newPosY] = C.Solid.Empty;
        }

        /// <summary>
        /// Adds a solid (where emptyness is not forced).
        /// </summary>
        /// <param name="levelInfo"></param>
        private void ModifyAdd(PuzzleInfo levelInfo)
        {
            int numTries = 0;
            int newPosX;
            int newPosY;
            do
            {
                numTries++;
                // Choose new square
                newPosX = Utility.Random(levelInfo.Width - 2) + 1;
                newPosY = Utility.Random(levelInfo.Height);
            } while (numTries < 100 && !(levelInfo.Solids[newPosX, newPosY] == C.Solid.Empty));

            // Change the value of one square
            levelInfo.Solids[newPosX, newPosY] = C.Solid.Solid;
        }

        /// <summary>
        /// Moves an arbitrary plank, but not the starting one.
        /// </summary>
        /// <param name="levelInfo"></param>
        private void ModifyPlanks(PuzzleInfo levelInfo)
        {
            Plank plank;
            do
            {
                plank = levelInfo.Planks[Utility.Random(levelInfo.Planks.Count)];
            } while (plank.Start.X == 0);
            RemoveForceSolid(levelInfo, plank);
            
            // Find a new place for the plank
            var possibleSolids = levelInfo.GetSolidList().FindAll(solid => solid.X != 0 && solid.X != levelInfo.Width - 1)
                                                         .FindAll(solid => solid.X < levelInfo.Width - 2 - plank.Length || solid.Y < levelInfo.Height - plank.Length);
            do
            {
                // Choose new square
                plank.Start = possibleSolids[Utility.Random(possibleSolids.Count)].Clone();

                // Choose new direction
                if (plank.Start.X >= levelInfo.Width - 2 - plank.Length)
                {
                    plank.Dir = C.DIR.S;
                }
                else if (plank.Start.Y >= levelInfo.Height - plank.Length)
                {
                    plank.Dir = C.DIR.E;
                }
                else
                {
                    plank.Dir = (C.DIR)(Utility.Random(2) + 1); // either east or south
                }
            } while (!MayPlacePlank(levelInfo, plank));

            // Mark new plank position as sqecial squares
            AddForceSolid(levelInfo, plank);
        }

        /// <summary>
        /// Checks whether one may place the plank given the solids in the levelInfo.
        /// </summary>
        /// <param name="levelInfo"></param>
        /// <param name="plank"></param>
        /// <returns></returns>
        private bool MayPlacePlank(PuzzleInfo levelInfo, Plank plank)
        {
            // Check solid points
            Point pos = plank.Start.Clone();
            if (levelInfo.Solids[pos.X, pos.Y].In(C.Solid.Empty, C.Solid.ForceEmpty)) return false;

            for (int i = 1; i < plank.Length; i++)
            {
                pos = pos.Move(plank.Dir);
                if (levelInfo.Solids[pos.X, pos.Y] != C.Solid.Empty) return false;
            }
            pos = pos.Move(plank.Dir);
            if (levelInfo.Solids[pos.X, pos.Y].In(C.Solid.Empty, C.Solid.ForceEmpty)) return false;

            // Check whether a plank of length 2 at the exact position exists.
            // All other intersections were already detected by intermediate points that were ForceEmpty.
            if (plank.Length == 2 && levelInfo.Planks.Exists(pla => pla.Length == 2 && pla != plank && pla.Dir == plank.Dir && pla.Start.Equals(plank.Start))) return false;
            return true;
        }


        /// <summary>
        /// Removes all forced points that were due to the plank.
        /// </summary>
        /// <param name="levelInfo"></param>
        /// <param name="plank"></param>
        protected void RemoveForceSolid(PuzzleInfo levelInfo, Plank plank)
        {
            Point pos = plank.Start.Clone();
            if (!levelInfo.Planks.Exists(pla => pla != plank && (pla.Start.Equals(pos) || pla.End.Equals(pos))))
            {
                levelInfo.Solids[pos.X, pos.Y] = C.Solid.Solid;
            }
            for (int i = 1; i < plank.Length; i++)
            {
                pos = pos.Move(plank.Dir);
                levelInfo.Solids[pos.X, pos.Y] = C.Solid.Empty;
            }
            pos = pos.Move(plank.Dir);
            if (!levelInfo.Planks.Exists(pla => pla != plank && (pla.Start.Equals(pos) || pla.End.Equals(pos))))
            {
                levelInfo.Solids[pos.X, pos.Y] = C.Solid.Solid;
            }
        }

        /// <summary>
        /// Adds forced points for the given plank.
        /// </summary>
        /// <param name="levelInfo"></param>
        /// <param name="plank"></param>
        protected void AddForceSolid(PuzzleInfo levelInfo, Plank plank)
        {
            Point pos = plank.Start.Clone();
            levelInfo.Solids[pos.X, pos.Y] = C.Solid.ForceSolid;
            for (int i = 1; i < plank.Length; i++)
            {
                pos = pos.Move(plank.Dir);
                levelInfo.Solids[pos.X, pos.Y] = C.Solid.ForceEmpty;
            }
            pos = pos.Move(plank.Dir);
            levelInfo.Solids[pos.X, pos.Y] = C.Solid.ForceSolid;
        }
    }
}
