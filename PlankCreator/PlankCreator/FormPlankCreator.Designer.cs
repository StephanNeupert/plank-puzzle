﻿namespace PlankCreator
{
    partial class FormPlankCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.butSolve = new System.Windows.Forms.Button();
            this.lblOutput = new System.Windows.Forms.Label();
            this.butCreateNew = new System.Windows.Forms.Button();
            this.txtWidth = new System.Windows.Forms.TextBox();
            this.lblWidth = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.lblPlankNum = new System.Windows.Forms.Label();
            this.txtPlankNum = new System.Windows.Forms.TextBox();
            this.lblFillRatio = new System.Windows.Forms.Label();
            this.txtFillRatio = new System.Windows.Forms.TextBox();
            this.lblPlankLen = new System.Windows.Forms.Label();
            this.lblNumTries = new System.Windows.Forms.Label();
            this.txtNumTries = new System.Windows.Forms.TextBox();
            this.butModify = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // butSolve
            // 
            this.butSolve.Location = new System.Drawing.Point(10, 10);
            this.butSolve.Name = "butSolve";
            this.butSolve.Size = new System.Drawing.Size(68, 48);
            this.butSolve.TabIndex = 0;
            this.butSolve.Text = "Solve Level";
            this.butSolve.UseVisualStyleBackColor = true;
            this.butSolve.Click += new System.EventHandler(this.butSolve_Click);
            // 
            // lblOutput
            // 
            this.lblOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutput.Location = new System.Drawing.Point(45, 215);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(192, 46);
            this.lblOutput.TabIndex = 1;
            this.lblOutput.Text = "Waiting...";
            this.lblOutput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // butCreateNew
            // 
            this.butCreateNew.Location = new System.Drawing.Point(84, 10);
            this.butCreateNew.Name = "butCreateNew";
            this.butCreateNew.Size = new System.Drawing.Size(68, 48);
            this.butCreateNew.TabIndex = 1;
            this.butCreateNew.Text = "Create Puzzle";
            this.butCreateNew.UseVisualStyleBackColor = true;
            this.butCreateNew.Click += new System.EventHandler(this.butCreateNew_Click);
            // 
            // txtWidth
            // 
            this.txtWidth.Location = new System.Drawing.Point(10, 89);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(68, 20);
            this.txtWidth.TabIndex = 3;
            this.txtWidth.Text = "6";
            this.txtWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblWidth
            // 
            this.lblWidth.Location = new System.Drawing.Point(10, 70);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(68, 16);
            this.lblWidth.TabIndex = 4;
            this.lblWidth.Text = "Width";
            this.lblWidth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHeight
            // 
            this.lblHeight.Location = new System.Drawing.Point(84, 70);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(68, 16);
            this.lblHeight.TabIndex = 6;
            this.lblHeight.Text = "Height";
            this.lblHeight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtHeight
            // 
            this.txtHeight.Location = new System.Drawing.Point(84, 89);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(68, 20);
            this.txtHeight.TabIndex = 5;
            this.txtHeight.Text = "6";
            this.txtHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblPlankNum
            // 
            this.lblPlankNum.Location = new System.Drawing.Point(158, 70);
            this.lblPlankNum.Name = "lblPlankNum";
            this.lblPlankNum.Size = new System.Drawing.Size(68, 16);
            this.lblPlankNum.TabIndex = 8;
            this.lblPlankNum.Text = "# Planks";
            this.lblPlankNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPlankNum
            // 
            this.txtPlankNum.Location = new System.Drawing.Point(158, 89);
            this.txtPlankNum.Name = "txtPlankNum";
            this.txtPlankNum.Size = new System.Drawing.Size(68, 20);
            this.txtPlankNum.TabIndex = 7;
            this.txtPlankNum.Text = "3";
            this.txtPlankNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblFillRatio
            // 
            this.lblFillRatio.Location = new System.Drawing.Point(232, 70);
            this.lblFillRatio.Name = "lblFillRatio";
            this.lblFillRatio.Size = new System.Drawing.Size(68, 16);
            this.lblFillRatio.TabIndex = 10;
            this.lblFillRatio.Text = "Fill-Ratio";
            this.lblFillRatio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFillRatio
            // 
            this.txtFillRatio.Location = new System.Drawing.Point(232, 89);
            this.txtFillRatio.Name = "txtFillRatio";
            this.txtFillRatio.Size = new System.Drawing.Size(68, 20);
            this.txtFillRatio.TabIndex = 9;
            this.txtFillRatio.Text = "30";
            this.txtFillRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblPlankLen
            // 
            this.lblPlankLen.Location = new System.Drawing.Point(10, 112);
            this.lblPlankLen.Name = "lblPlankLen";
            this.lblPlankLen.Size = new System.Drawing.Size(95, 16);
            this.lblPlankLen.TabIndex = 12;
            this.lblPlankLen.Text = "Plank Lengths";
            this.lblPlankLen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumTries
            // 
            this.lblNumTries.Location = new System.Drawing.Point(232, 19);
            this.lblNumTries.Name = "lblNumTries";
            this.lblNumTries.Size = new System.Drawing.Size(68, 16);
            this.lblNumTries.TabIndex = 14;
            this.lblNumTries.Text = "# Tries";
            this.lblNumTries.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNumTries
            // 
            this.txtNumTries.Location = new System.Drawing.Point(232, 38);
            this.txtNumTries.Name = "txtNumTries";
            this.txtNumTries.Size = new System.Drawing.Size(68, 20);
            this.txtNumTries.TabIndex = 13;
            this.txtNumTries.Text = "100";
            this.txtNumTries.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // butModify
            // 
            this.butModify.Location = new System.Drawing.Point(158, 10);
            this.butModify.Name = "butModify";
            this.butModify.Size = new System.Drawing.Size(68, 48);
            this.butModify.TabIndex = 15;
            this.butModify.Text = "Modify Puzzle";
            this.butModify.UseVisualStyleBackColor = true;
            this.butModify.Click += new System.EventHandler(this.butModify_Click);
            // 
            // FormPlankCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 273);
            this.Controls.Add(this.butModify);
            this.Controls.Add(this.lblNumTries);
            this.Controls.Add(this.txtNumTries);
            this.Controls.Add(this.lblPlankLen);
            this.Controls.Add(this.lblFillRatio);
            this.Controls.Add(this.txtFillRatio);
            this.Controls.Add(this.lblPlankNum);
            this.Controls.Add(this.txtPlankNum);
            this.Controls.Add(this.lblHeight);
            this.Controls.Add(this.txtHeight);
            this.Controls.Add(this.lblWidth);
            this.Controls.Add(this.txtWidth);
            this.Controls.Add(this.butCreateNew);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.butSolve);
            this.Name = "FormPlankCreator";
            this.Text = "Plank Puzzle Creator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button butSolve;
        private System.Windows.Forms.Label lblOutput;
        private System.Windows.Forms.Button butCreateNew;
        private System.Windows.Forms.TextBox txtWidth;
        private System.Windows.Forms.Label lblWidth;
        private System.Windows.Forms.Label lblHeight;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.Label lblPlankNum;
        private System.Windows.Forms.TextBox txtPlankNum;
        private System.Windows.Forms.Label lblFillRatio;
        private System.Windows.Forms.TextBox txtFillRatio;
        private System.Windows.Forms.Label lblPlankLen;
        private System.Windows.Forms.Label lblNumTries;
        private System.Windows.Forms.TextBox txtNumTries;
        private System.Windows.Forms.Button butModify;
    }
}

