﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace PlankCreator
{
    class LevelInfo : IEquatable<LevelInfo>
    {
        public int[] PlankInfo;
        public List<Move> MoveList;
        public int HashSum { get; private set; }

        public LevelInfo(int[] planks, List<Move> moves)
        {
            this.PlankInfo = planks;
            this.MoveList = new List<Move>(moves);
            this.HashSum = this.PlankInfo.Sum();
        }

        public bool Equals(LevelInfo otherLevel)
        {
            for (int i = 0; i < this.PlankInfo.Length; i++)
            {
                if (this.PlankInfo[i] != otherLevel.PlankInfo[i]) return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            return HashSum;
        }
    }

    class Solver
    {
        public Solver(Level level)
        {
            this.level = level;
            var levelInfo = CreateLevelInfo();
            allConstellations = new HashSet<LevelInfo>();
            curConstellations = new HashSet<LevelInfo>();
            newConstellations = new HashSet<LevelInfo>();
            // add starting position
            allConstellations.Add(levelInfo);
            curConstellations.Add(levelInfo);
        }

        private Level level;
        private LevelInfo solution;
        private HashSet<LevelInfo> allConstellations;
        private HashSet<LevelInfo> curConstellations;
        private HashSet<LevelInfo> newConstellations;
        private List<List<Move>> moveList; // only for NoTarget-Solver
        
        /// <summary>
        /// Translates the level into a LevelInfo.
        /// </summary>
        /// <returns></returns>
        public LevelInfo CreateLevelInfo()
        {
            // First simplify plank orientation
            level.Planks.FindAll(pla => pla.Dir.In(C.DIR.N, C.DIR.W))
                        .ForEach(pla => pla = pla.Reverse());
            
            // Create SaveArray
            int[] PlankArr = new int[level.Planks.Count];
            for (int i = 0; i < level.Planks.Count; i++)
            {
                // bit 0-2: Length
                // bit 3: Reachable
                // bit 4-5: Direction 
                // bit 6-11: GridPosY
                // bit 12-17: GridPosX
                int NewInt = (level.Planks[i].Start.X << 12) +
                             (level.Planks[i].Start.Y << 6) +
                             ((int)level.Planks[i].Dir << 4) +
                              level.Planks[i].Length;
                if (level.Planks[i].IsReachable) NewInt += 8;

                PlankArr[i] = NewInt;
            }
              
            return new LevelInfo(PlankArr, level.Moves);
        }

        /// <summary>
        /// Loads a level from the given level infos.
        /// </summary>
        /// <param name="levelInfo"></param>
        public void LoadLevel(LevelInfo levelInfo)
        {
            for (int i = 0; i < levelInfo.PlankInfo.Length; i++)
            {
                int NewInt = levelInfo.PlankInfo[i];
                level.Planks[i].Start.X = (NewInt >> 12) & 63;
                level.Planks[i].Start.Y = (NewInt >> 6) & 63;
                level.Planks[i].Dir = (C.DIR)((NewInt >> 4) & 3);
                if (((NewInt >> 3) & 1) == 1) level.Planks[i].IsReachable = true;
                else level.Planks[i].IsReachable = false;
                level.Planks[i].Length = NewInt & 7;
            }

            level.Moves = levelInfo.MoveList;
        }

        /// <summary>
        /// Solved a given level and returns a list of solving moves.
        /// <para> Returns null, if the level is unsolvable. </para>
        /// </summary>
        /// <returns></returns>
        public List<Move> SolveLevel()
        {
            while (curConstellations.Count > 0 && solution == null)
            {
                newConstellations = new HashSet<LevelInfo>();

                foreach (LevelInfo levelInfo in curConstellations)
                {
                    LoadLevel(levelInfo);
                    TryMoves();
                }

                curConstellations = newConstellations;
                allConstellations.UnionWith(newConstellations);
            }

            return solution?.MoveList;
        }

        /// <summary>
        /// Solves a level for all possible final target positions.
        /// </summary>
        /// <returns></returns>
        public List<List<Move>> SolveLevelNoTarget()
        {
            moveList = new List<List<Move>>(level.Height);
            for (int i = 0; i < level.Height; i++) moveList.Add(null);

            // Make moves, until level is solved, or we don't have any new positions
            while (curConstellations.Count > 0 && moveList.Count(moves => moves != null) < level.Height)
            {
                newConstellations = new HashSet<LevelInfo>();

                foreach (LevelInfo levelInfo in curConstellations)
                {
                    LoadLevel(levelInfo);
                    TryMoves();
                }

                curConstellations = newConstellations;
                allConstellations.UnionWith(newConstellations);
            }

            return moveList;
        }

        /// <summary>
        /// Tries all moves that can be done in the current situation.
        /// </summary>
        private void TryMoves()
        { 
            bool HasMoved;

            // Get all reachable squares
            HashSet<Point> reachSolids = level.GetReachableSolids();

            // Loop over all reachable squares, reachable planks and directions
            foreach (Plank plank in level.Planks.FindAll(p => p.IsReachable)) 
            {
                foreach (Point solid in reachSolids)
                { 
                    foreach (C.DIR dir in C.Dirs)
                    {
                        HasMoved = level.SetPlank(plank, solid, dir);
                           
                        if (HasMoved)
                        {
                            CheckNewLevel(plank);
                            level.UndoMove();
                        }
                    }
                }          
            }
        }

        /// <summary>
        /// Checks whether we raeched the goal or a new constellation.
        /// </summary>
        /// <param name="movePlank"></param>
        private void CheckNewLevel(Plank movePlank)
        {
            if (movePlank.End.X == level.Width - 1)
            {
                // Check whether we already have reached this place
                int finalPosY = movePlank.End.Y;
                if (moveList != null && finalPosY < moveList.Count && moveList[finalPosY] == null)
                {
                    moveList[finalPosY] = new List<Move>(level.Moves);
                }

                if (level.IsLevelSolved()) // Should automatically be fulfilled coming from LevelSolver
                {
                    solution = CreateLevelInfo();
                }
            }
            else
            {
                LevelInfo newLevelInfo = CreateLevelInfo();
                if (!allConstellations.Contains(newLevelInfo))
                {
                    newConstellations.Add(newLevelInfo);
                }
            }
        }


    }
}
