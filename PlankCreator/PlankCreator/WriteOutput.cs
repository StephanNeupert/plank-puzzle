﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace PlankCreator
{
    static class WriteOutput
    {
        /// <summary>
        /// Saves a sequence of moves to "AutoSolutions.txt".
        /// </summary>
        /// <param name="moves"></param>
        public static void SaveSolution(List<Move> moves)
        {
            string filePath = C.AppPath + "AutoSolutions.txt";
            EnsureFileExists(filePath);

            // Add the string to the Solution.txt file
            using (TextWriter fileWriter = new StreamWriter(filePath, true))
            {
                string title = "// Plank Moves: " + moves?.Count.ToString() + "   Score: " + moves?.Score().ToString();
                fileWriter.WriteLine(title);
                CreateMoveString(moves).ForEach(solstr => fileWriter.WriteLine("// " + solstr));
            }
        }

        /// <summary>
        /// Ensures that a given file exists.
        /// </summary>
        /// <param name="filePath"></param>
        private static void EnsureFileExists(string filePath)
        {
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
            }
        }

        /// <summary>
        /// Creates a list of string specifying the solution.
        /// </summary>
        /// <param name="moves"></param>
        /// <returns></returns>
        private static List<string> CreateMoveString(List<Move> moves)
        {
            List<string> solStrings = new List<string>();
            
            // Create the solution string and write level number
            StringBuilder solution = new StringBuilder();

            // Check for unsolvable levels
            if (moves?.Count == 0)
            {
                solution.Append("unsolvable");
            }

            // Write correct string for solved levels 
            // (this doesn't add anything if it isn't solved)
            for (int i = 0; i < moves?.Count; i++)
            {
                solution.Append(" ");
                solution.Append(moves[i].NewPos.X.ToString().PadLeft(2));
                solution.Append(moves[i].NewPos.Y.ToString().PadLeft(2));
                solution.Append(moves[i].Direction.ToString());
                solution.Append("  - ");

                // Add the solution in regular intervals to the SolutionList
                if ((i + 1) % 10 == 0 || (i + 1) == moves.Count)
                {
                    solStrings.Add(solution.ToString());
                    solution.Clear();
                }
                // Add clear lines every 50 moves
                if ((i + 1) % 50 == 0)
                {
                    solStrings.Add(" ");
                }
            }

            return solStrings;
        }


        public static void SaveLevel(PuzzleInfo levelInfo, string fileName)
        {
            string filePath = C.AppPath + fileName + ".txt";
            File.Create(filePath).Close();

            using (TextWriter fileWriter = new StreamWriter(filePath, true))
            {
                // Write global info for user
                fileWriter.WriteLine("// Size " + (levelInfo.Width - 2).ToString() + "x" + levelInfo.Height.ToString());
                fileWriter.WriteLine("// Score: " + levelInfo.Score.ToString().PadLeft(3));
                fileWriter.WriteLine("// Plank Moves: " + levelInfo.Moves.Count.ToString().PadLeft(3));
                fileWriter.WriteLine(" ");
                
                // Write global info to read for the puzzle game
                fileWriter.WriteLine("SIZ " + levelInfo.Width.ToString().PadLeft(2) + levelInfo.Height.ToString().PadLeft(3));
                fileWriter.WriteLine("STA  0" + levelInfo.StartPosY.ToString().PadLeft(3));
                fileWriter.WriteLine("END " + (levelInfo.Width - 1).ToString().PadLeft(2) + levelInfo.TargetPosY.ToString().PadLeft(3));
                fileWriter.WriteLine(" ");

                // Write solid squares
                for (int y = 0; y < levelInfo.Height; y++)
                {
                    StringBuilder solidLine = new StringBuilder("SOL");
                    solidLine.Append(y.ToString().PadLeft(3));
                    solidLine.Append("    ");
                    for (int x = 0; x < levelInfo.Width; x++)
                    {
                        if (levelInfo.Solids[x, y].In(C.Solid.Solid, C.Solid.ForceSolid))
                        {
                            solidLine.Append(" 1");
                        }
                        else
                        {
                            solidLine.Append(" 0");
                        }
                    }
                    fileWriter.WriteLine(solidLine.ToString());
                }
                fileWriter.WriteLine(" ");

                // Write planks
                foreach (Plank plank in levelInfo.Planks)
                {
                    string plankLine = "PLA " + plank.Start.X.ToString().PadLeft(2)
                                              + plank.Start.Y.ToString().PadLeft(3)
                                              + plank.Dir.ToString().PadLeft(2)
                                              + plank.Length.ToString().PadLeft(2);
                    fileWriter.WriteLine(plankLine);
                }
                fileWriter.WriteLine(" ");

                // Write Solution
                fileWriter.WriteLine("// Solution");
                CreateMoveString(levelInfo.Moves).ForEach(solstr => fileWriter.WriteLine("// " + solstr));


            }


        }
    }
}
