﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

namespace PlankCreator
{
    static class C // for Constants
    {
        public static char DirSep => System.IO.Path.DirectorySeparatorChar;
        public static string NewLine => Environment.NewLine;

        public static string AppPath => Application.StartupPath + DirSep;
        public static string PuzzlePath => C.AppPath + "LevelNew.txt";

        public enum DIR { N, E, S, W }
        public static readonly DIR[] Dirs = new DIR[] { DIR.N, DIR.E, DIR.S, DIR.W };

        public enum MoveType { Walk, Take, Set }

        public enum Solid { ForceEmpty, Empty, Solid, ForceSolid}

        public const string Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }

    class BufferedForm : Form
    {
        public BufferedForm()
        {
            this.DoubleBuffered = true;
            this.ResizeRedraw = true;
        }
    }

    static class Utility
    {
        private static Random rnd;
        public static void InitializeRandom()
        {
            rnd = new Random();
        } 
        public static int Random(int max) => rnd.Next(max);

        /// <summary>
        /// Checks if an object is contained in an array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool In<T>(this T obj, params T[] args)
        {
            return args.Contains(obj);
        }

        /// <summary>
        /// Parses a string value to an enum of given type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        /// <summary>
        /// Creates a deep clone of a point.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Point Clone(this Point point)
        {
            return new Point(point.X, point.Y);
        }

        /// <summary>
        /// Moves a point in a given direction.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static Point Move(this Point point, C.DIR direction)
        {
            return point.Move(direction, 1);
        }

        /// <summary>
        /// Moves a point in a given direction for a given length.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="direction"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static Point Move(this Point point, C.DIR direction, int length)
        {
            switch (direction)
            {
                case C.DIR.N: point.Y -= length; break;
                case C.DIR.E: point.X += length; break;
                case C.DIR.S: point.Y += length; break;
                case C.DIR.W: point.X -= length; break;
            }
            return point;
        }

        /// <summary>
        /// Returns the opposite direction.
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static C.DIR Opposite(this C.DIR direction)
        {
            switch (direction)
            {
                case C.DIR.N: return C.DIR.S;
                case C.DIR.E: return C.DIR.W;
                case C.DIR.S: return C.DIR.N;
                case C.DIR.W: return C.DIR.E;
                default: return C.DIR.N;
            }
        }

        /// <summary>
        /// Opens the file browser and returns the selected files.
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="startFolder"></param>
        /// <returns></returns>
        public static string SelectFile(string filter, string startFolder = null)
        {
            var openFileDialog = new OpenFileDialog();

            openFileDialog.InitialDirectory = (startFolder != null) ? startFolder : C.AppPath;
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = filter;
            openFileDialog.RestoreDirectory = true;
            openFileDialog.CheckFileExists = true;

            string filePath = string.Empty;

            try
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFileDialog.FileName;
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Error while showing the file browser." + C.NewLine + Ex.Message, "File browser error");
            }
            finally
            {
                openFileDialog?.Dispose();
            }

            return filePath;
        }

        /// <summary>
        /// Computes the score of a list of moves.
        /// </summary>
        /// <param name="moves"></param>
        /// <returns></returns>
        public static int Score(this List<Move> moves)
        {
            if (moves == null) return -100;

            int repeats = 0;
            int score = 2;

            for (int i = 1; i < moves.Count; i++)
            {
                // Update Repeats
                if (moves[i].PlankIndex == moves[i - 1].PlankIndex) repeats++;
                else repeats = 0;

                // Update score depending on number of repeats
                score += 2 - repeats;

                // Add additional bonus for large planks
                if (moves[i].PlankLength == 3) score++;
                else if (moves[i].PlankLength > 3) score += 2;

                // Deduct points for moveing more than twice with a plank of length 1
                if (moves[i].PlankLength == 1) score -= repeats / 2;
            }

            return score;
        }

        /// <summary>
        /// Handles a global unexpected exception and displays a warning message to the user.
        /// </summary>
        /// <param name="Ex"></param>
        public static void HandleGlobalException(Exception Ex)
        {
            try
            {
                LogException(Ex);
                string errorString = "An error occured: " + Ex.Message + C.NewLine + "Try to continue? Selecting 'no' will quit the puzzle creator.";
                var result = MessageBox.Show(errorString, "Error", MessageBoxButtons.YesNo);
                if (result == DialogResult.No) Application.Exit();
            }
            catch
            {
                Application.Exit();
            }
        }

        /// <summary>
        /// Logs an exception message to AppPath/ErrorLog.txt.
        /// </summary>
        /// <param name="ex"></param>
        public static void LogException(Exception ex)
        {
            string errorPath = C.AppPath + "ErrorLog.txt";
            using (System.IO.TextWriter textFile = new System.IO.StreamWriter(errorPath, true))
            {
                textFile.WriteLine(ex.ToString());
            }
        }
    }
}
