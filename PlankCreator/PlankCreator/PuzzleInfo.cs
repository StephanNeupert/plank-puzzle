﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PlankCreator
{
    /// <summary>
    /// Contains all important information about a puzzle
    /// </summary>
    class PuzzleInfo
    {
        public PuzzleInfo(List<Plank> planks, C.Solid[,] solids)
        {
            // Create a deep copy of the plank list
            this.Planks = planks.ConvertAll(p => p.Clone());

            // Create a deep copy of the solids array
            int width = solids.GetLength(0);
            int height = solids.GetLength(1);
            this.Solids = new C.Solid[width, height];
            for (int x = 0; x < width - 1; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    this.Solids[x, y] = solids[x, y];
                }
            }

            // Use default values for everything else.
            for (int y = 0; y < height; y++) this.Solids[width - 1, y] = C.Solid.Empty;
            this.Moves = null;
        }

        public PuzzleInfo(List<Plank> planks, C.Solid[,] solids, List<Move> moves)
                : this(planks, solids)
        {
            this.Moves = moves;
            if (moves != null && moves.Count > 0)
            {
                int targetPosY = moves.Last().NewPos.Y;
                this.Solids[Width - 1, targetPosY] = C.Solid.Solid;
            }
        }

        public PuzzleInfo(Level level)
        {
            // Create a deep copy of the plank list
            this.Planks = level.Planks.ConvertAll(p => p.Clone());

            // Create a deep copy of the solids array
            this.Solids = new C.Solid[level.Width, level.Height];
            for (int x = 0; x < level.Width - 1; x++)
            {
                for (int y = 0; y < level.Height; y++)
                {
                    this.Solids[x, y] = C.Solid.Empty;
                }
            }
            foreach (System.Drawing.Point solid in level.Solids)
            {
                this.Solids[solid.X, solid.Y] = C.Solid.Solid;
            }

            this.Moves = level.Moves;
        }

        public List<Plank> Planks;
        public C.Solid[,] Solids; // giving Width and Height as well
        public List<Move> Moves;
        
        public int Width => Solids.GetLength(0);
        public int Height => Solids.GetLength(1);
        public int StartPosY => Enumerable.Range(0, Height).First(y => Solids[0, y].In(C.Solid.Solid, C.Solid.ForceSolid));
        public int TargetPosY => Enumerable.Range(0, Height).FirstOrDefault(y => Solids[Width - 1, y].In(C.Solid.Solid, C.Solid.ForceSolid));
        public int Score => Moves.Score();

        public PuzzleInfo Clone()
        {
            return new PuzzleInfo(Planks, Solids, Moves);
        }

        /// <summary>
        /// Returns a hashset of all solid points.
        /// </summary>
        /// <returns></returns>
        public HashSet<System.Drawing.Point> GetSolidSet()
        {
            var solids = new HashSet<System.Drawing.Point>();
            for (int y = 0; y < Height; y++)
            {
                solids.Add(new System.Drawing.Point(Width - 1, y));
            }

            for (int x = 0; x < Width - 1; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (Solids[x, y].In(C.Solid.Solid, C.Solid.ForceSolid))
                    {
                        solids.Add(new System.Drawing.Point(x, y));
                    }
                }
            }
            return solids;
        }

        /// <summary>
        /// Returns a list of all solid points.
        /// </summary>
        /// <param name="includeForced"></param>
        /// <returns></returns>
        public List<System.Drawing.Point> GetSolidList(bool includeForced = true)
        {
            var solids = new List<System.Drawing.Point>();
            if (includeForced)
            {
                for (int y = 0; y < Height; y++)
                {
                    solids.Add(new System.Drawing.Point(Width - 1, y));
                }
            }

            for (int x = 0; x < Width - 1; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (  (Solids[x, y] == C.Solid.Solid)
                        ||(Solids[x, y] == C.Solid.ForceSolid && includeForced))
                    {
                        solids.Add(new System.Drawing.Point(x, y));
                    }
                }
            }
            return solids;
        }
    }
}
