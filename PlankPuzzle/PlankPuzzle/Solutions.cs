﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PlankPuzzle
{
    class Statistics : IComparable<Statistics>
    {
        public int NumPlankSets;
        public int NumMoves;

        public Statistics() { }

        public int CompareTo(Statistics newStats)
        {
            if (newStats == null) return 1;
            else if (NumPlankSets == newStats.NumPlankSets) return NumMoves.CompareTo(newStats.NumMoves);
            else return NumPlankSets.CompareTo(newStats.NumPlankSets);
        }
    }

    class Solutions
    {
        /// <summary>
        /// Saves a new solution to the solution file.
        /// </summary>
        /// <param name="moveList"></param>
        /// <param name="levelName"></param>
        static public void SaveSolution(List<Move> moveList, string levelName)
        {
            EnsureFileExists(C.SolutionPath);

            string solution = CreateSolutionString(moveList, levelName);
            if (!CheckSolutionExists(solution))
            {
                using (TextWriter TextFile = new StreamWriter(C.SolutionPath, true))
                {
                    TextFile.WriteLine(solution);
                }
            }
        }

        /// <summary>
        /// Checks whether the file exists and creates it if necessary.
        /// </summary>
        /// <param name="filePath"></param>
        static private void EnsureFileExists(string filePath)
        {
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
            }
        }

        /// <summary>
        /// Creates the string representing the solution.
        /// </summary>
        /// <param name="moveList"></param>
        /// <param name="levelName"></param>
        /// <returns></returns>
        static private string CreateSolutionString(List<Move> moveList, string levelName)
        {
            StringBuilder solution = new StringBuilder(levelName + ":  ");

            foreach (Move move in moveList)
            {
                switch (move.MoveType)
                {
                    case C.MoveType.Walk: solution.Append('.'); break;
                    case C.MoveType.Take: solution.Append('t'); break;
                    case C.MoveType.Set: solution.Append('s'); break;
                }

                solution.Append(move.Direction.ToString());
            }

            return solution.ToString();
        }

        /// <summary>
        /// Returns whether a solution already exists withing the solution file.
        /// </summary>
        /// <param name="solution"></param>
        /// <returns></returns>
        static private bool CheckSolutionExists(string solution)
        {
            try
            {
                // Open the text file using a stream reader.
                using (StreamReader fileReader = new StreamReader(C.SolutionPath))
                {
                    string line;
                    while ((line = fileReader.ReadLine()) != null)
                    {
                        if (line == solution)
                        {
                            return true;
                        }
                    }
                }
            }
            catch
            {
                // nothing
            }

            return false;
        }

        /// <summary>
        /// Gets the statistics of the best solution to the level.
        /// </summary>
        /// <param name="levelName"></param>
        /// <returns></returns>
        static public Statistics GetSolutionStats(string levelName)
        {
            if (!File.Exists(C.SolutionPath)) return null;

            var stats = GetSolutionLines(levelName).ConvertAll(sol => GetStatistics(sol));
            stats.Sort();
            return stats.FirstOrDefault();
        }

        /// <summary>
        /// Reads all solutions to a level from the solution file.
        /// </summary>
        /// <param name="levelName"></param>
        /// <returns></returns>
        static private List<string> GetSolutionLines(string levelName)
        {
            List<string> solutions = new List<string>();

            try
            {
                using (StreamReader fileReader = new StreamReader(C.SolutionPath))
                {
                    string line;
                    while ((line = fileReader.ReadLine()) != null)
                    {
                        if (line.StartsWith(levelName))
                        {
                            solutions.Add(line);
                        }
                    }
                }
            }
            catch
            {
                //nothing
            }

            return solutions;
        }

        static private Statistics GetStatistics(string Line)
        {
            Statistics Stats = new Statistics();

            for (int i = 0; i < Line.Length; i++)
            {
                if (Line[i] == '.') Stats.NumMoves++;
                else if (Line[i] == 's') Stats.NumPlankSets++;
            }
            return Stats;
        }

    }
}
