﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.IO;

namespace PlankPuzzle
{
    static class LevelLoader
    {
        /// <summary>
        /// Loads a new level from a file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static Level LoadFromFile(string filePath)
        {
            Level level = new Level();
            level.PlankList = new List<Plank>();
            level.SolidList = new List<Point>();
            level.MoveList = new List<Move>();

            // Read in all lines
            List<string> fileStrings = ReadFile(filePath);
            fileStrings.ForEach(str => EvaluateLine(level, str));

            SanitizeInput(level);

            return level;
        }

        /// <summary>
        /// Reads in all files of the level.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static List<string> ReadFile(string filePath)
        {
            List<string> fileLines = new List<string>();

            try
            {
                // Open the text file using a stream reader.
                using (StreamReader fileReader = new StreamReader(filePath))
                {
                    string line;
                    while ((line = fileReader.ReadLine()) != null)
                    {
                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            fileLines.Add(line);
                        }
                    }
                }
            }
            catch
            {
                // Load default empty level
                fileLines.Add("Siz 3 3");
                fileLines.Add("Sta 0 0");
                fileLines.Add("End 2 2");
            }

            return fileLines;
        }

        /// <summary>
        /// Reads the level info from a single line.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="line"></param>
        private static void EvaluateLine(Level level, string line)
        {
            try
            {
                if (line.Length < 3) return;

                switch (line.Substring(0, 3).ToUpper())
                {
                    case "SIZ": ReadSizeLine(level, SplitLine(line)); break;
                    case "STA": ReadPlayerPosLine(level, SplitLine(line)); break;
                    case "END": ReadTargetPosLine(level, SplitLine(line)); break;
                    case "SOL": ReadSolidLine(level, SplitLine(line)); break;
                    case "PLA": ReadPlankLine(level, SplitLine(line)); break;
                }
            }
            catch
            {
                // silent fail!
            }
        }

        /// <summary>
        /// Separates the various info components of a line.
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private static List<string> SplitLine(string line)
        {
            List<string> infos = line.Split(' ').ToList();
            infos.RemoveAll(str => string.IsNullOrWhiteSpace(str));
            return infos;
        }

        /// <summary>
        /// Reads the size of the level from a line.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="infos"></param>
        private static void ReadSizeLine(Level level, List<string> infos)
        {
            level.Width = int.Parse(infos[1]);
            level.Height = int.Parse(infos[2]);
        }

        /// <summary>
        /// Reads the start position for the player from a line.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="infos"></param>
        private static void ReadPlayerPosLine(Level level, List<string> infos)
        {
            level.PlayerPos = new Point(int.Parse(infos[1]), int.Parse(infos[2]));
        }

        /// <summary>
        /// Reads the target position for the player from a line.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="infos"></param>
        private static void ReadTargetPosLine(Level level, List<string> infos)
        {
            level.TargetPos = new Point(int.Parse(infos[1]), int.Parse(infos[2]));
        }

        /// <summary>
        /// Reads the info for a solid place from a line.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="infos"></param>
        private static void ReadSolidLine(Level level, List<string> infos)
        {
            int row = int.Parse(infos[1]);

            for (int column = 0; column < level.Width; column++)
            {
                if (int.Parse(infos[2 + column]) == 1)
                {
                    level.SolidList.Add(new Point(column, row));
                }
            }
        }

        /// <summary>
        /// Reads the position of a new plank from a line.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="infos"></param>
        private static void ReadPlankLine(Level level, List<string> infos)
        {
            Plank plank = new Plank();
            plank.Start = new Point(int.Parse(infos[1]), int.Parse(infos[2]));
            plank.Direction = Utility.ParseEnum<C.DIR>(infos[3]);
            plank.Length = int.Parse(infos[4]);

            level.PlankList.Add(plank);
        }

        /// <summary>
        /// Ensure that no invalid inputs are in the level data.
        /// </summary>
        /// <param name="level"></param>
        private static void SanitizeInput(Level level)
        {
            // Level size
            if (level.Width < 1) level.Width = 1;
            else if (level.Width > 20) level.Width = 20;

            if (level.Height < 1) level.Height = 1;
            else if (level.Height > 20) level.Height = 20;

            int width = level.Width;
            int height = level.Height;

            level.SolidList.RemoveAll(solid => solid.X < 0 || solid.X >= width
                                            || solid.Y < 0 || solid.Y >= height);


            level.PlankList.RemoveAll(pla => pla.Start.X < 0 || pla.Start.X >= width
                                          || pla.Start.Y < 0 || pla.Start.Y >= height
                                          || pla.End.X < 0 || pla.End.X >= width
                                          || pla.End.Y < 0 || pla.End.Y >= height);

            // Make sure StartPos and TargetPos are not null
            if (level.PlayerPos == null) level.PlayerPos = new Point(0, 0);
            if (level.TargetPos == null) level.TargetPos = new Point(0, 0);
        }


    }
}
