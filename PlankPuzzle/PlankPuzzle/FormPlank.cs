﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;

namespace PlankPuzzle
{
    partial class FormPlank : Form
    {
        public FormPlank()
        {
            InitializeComponent();
            CreateComponents();

            CreatePuzzleNameList();
            levelIndex = 0;
            LoadLevel();
            renderer.RenderLevel();
        }

        private List<string> puzzleNames;
        private int levelIndex;

        private Level startLevel;
        private Level curLevel;
        private Renderer renderer;

        private void CreateComponents()
        {
            var button = new MyButton(10, 626, 60, 40, "Restart Level");
            button.Click += new EventHandler(Button_RestartLevel_Click);
            Controls.Add(button);

            button = new MyButton(80, 626, 60, 40, "Previous Level");
            button.Click += new EventHandler(Button_PrevLevel_Click);
            Controls.Add(button);

            button = new MyButton(150, 626, 60, 40, "Next Level");
            button.Click += new EventHandler(Button_NextLevel_Click);
            Controls.Add(button);

            button = new MyButton(220, 626, 60, 40, "Undo Move");
            button.Click += new EventHandler(Button_UndoMove_Click);
            Controls.Add(button);
        }

        /// <summary>
        /// Load the sorted list of all levels.
        /// </summary>
        private void CreatePuzzleNameList()
        {
            puzzleNames = Directory.GetFiles(C.AppPath, "*.txt", SearchOption.TopDirectoryOnly)
                                   .Select(file => Path.GetFileName(file))
                                   .ToList()
                                   .FindAll(file => file.StartsWith("Level"));
            puzzleNames.Sort();
        }

        /// <summary>
        /// Updates the level title.
        /// </summary>
        private void DisplayLevelTitle(Statistics stats = null)
        {
            if (stats == null) stats = Solutions.GetSolutionStats(puzzleNames[levelIndex]);

            string title = "Level " + (levelIndex + 1).ToString();
            if (stats == null || stats.NumMoves == 0)
            {
                title += "  (unsolved)";
            }
            else 
            {
                title += "  (solved in " + stats.NumMoves.ToString() + " steps and "
                                + stats.NumPlankSets.ToString() + " plank movements)";
            }

            lblLevelTitle.Text = title;
        }

        /// <summary>
        /// Updates the screen to display the success, if appropriate.
        /// </summary>
        private void HandleLevelSolved()
        {
            if (curLevel.IsLevelSolved())
            {
                Solutions.SaveSolution(curLevel.MoveList, puzzleNames[levelIndex]);
                Statistics stats = Solutions.GetSolutionStats(puzzleNames[levelIndex]);
                DisplayLevelTitle(stats);
            }
        }

        /// <summary>
        /// Loads the level with the current index.
        /// </summary>
        private void LoadLevel()
        {
            if (levelIndex >= puzzleNames.Count) return;
            LoadLevel(puzzleNames[levelIndex]);
        }

        /// <summary>
        /// Loads the level with the given level name.
        /// </summary>
        /// <param name="levelName"></param>
        private void LoadLevel(string levelName)
        {
            if (!File.Exists(C.AppPath + levelName)) return;

            startLevel = LevelLoader.LoadFromFile(C.AppPath + levelName);
            curLevel = startLevel.Clone();
            renderer = new Renderer(curLevel, PicBoxLevel, PicBoxPlank);

            DisplayLevelTitle();
        }



        private void PicBoxLevel_MouseClick(object sender, MouseEventArgs e)
        {
            // Get Direction of the click
            C.DIR dir = renderer.GetDirectionFromClick(e.X, e.Y);
            MakeMove(dir, (e.Button == MouseButtons.Right));
        }


        private void FormMain_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    MakeMove(C.DIR.N, e.Control); break;
                case Keys.Right:
                    MakeMove(C.DIR.E, e.Control); break;
                case Keys.Down:
                    MakeMove(C.DIR.S, e.Control); break;
                case Keys.Left:
                    MakeMove(C.DIR.W, e.Control); break;
                case Keys.U:
                    Button_UndoMove_Click(null, null); break;
                case Keys.Space:
                    Button_RestartLevel_Click(null, null); break;
                case Keys.Enter:
                    if (curLevel.IsLevelSolved()) Button_NextLevel_Click(null, null); break;
            }
        }

        /// <summary>
        /// Moves the player.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="isCtrl"></param>
        private void MakeMove(C.DIR dir, bool isCtrl)
        {
            if (curLevel == null || curLevel.IsLevelSolved() || dir == C.DIR.Null) return;

            bool levelChanged = false;

            if (isCtrl)
            {
                // Try to pick up a plank, otherwise try to set one
                levelChanged = curLevel.TakePlank(dir);
                if (!levelChanged) levelChanged = curLevel.SetPlank(dir);
            }
            else
            {
                // Try to move along a plank, otherwise try to set a plank
                levelChanged = curLevel.MovePlayer(dir);
                if (!levelChanged) levelChanged = curLevel.SetPlank(dir);
            }

            if (levelChanged) renderer.RenderLevel();

            HandleLevelSolved();
        }

        /// <summary>
        /// Presents the next level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_NextLevel_Click(object sender, EventArgs e)
        {
            levelIndex = (levelIndex + 1) % puzzleNames.Count;
            LoadLevel();
            renderer.RenderLevel();
        }

        /// <summary>
        /// Presents the previous level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_PrevLevel_Click(object sender, EventArgs e)
        {
            levelIndex = (levelIndex - 1 + puzzleNames.Count) % puzzleNames.Count;
            LoadLevel();
            renderer.RenderLevel();
        }

        /// <summary>
        /// Restarts the level.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_RestartLevel_Click(object sender, EventArgs e)
        {
            curLevel = startLevel.Clone();
            renderer = new Renderer(curLevel, PicBoxLevel, PicBoxPlank);
            renderer.RenderLevel();
        }

        /// <summary>
        /// Undos the last move.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_UndoMove_Click(object sender, EventArgs e)
        {
            if (curLevel.MoveList.Count == 0) return;

            curLevel.UndoMove();
            renderer.RenderLevel();
        }

    }

}
