﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace PlankPuzzle
{
    class Renderer
    {
        public Renderer(Level level, PictureBox picBox, PictureBox picBoxPlank)
        {
            this.level = level;
            this.picBox = picBox;
            this.picBoxPlank = picBoxPlank;
        }

        private Level level;
        private PictureBox picBox;
        private PictureBox picBoxPlank;
        private Bitmap image;

        private int gridSize => level.GridSize;
        private int offset => gridSize / 8;

        /// <summary>
        /// Renders the current level and displays it on the form.
        /// </summary>
        public void RenderLevel()
        {
            // Create new image
            int imageWidth = 2 + gridSize / 4 + gridSize * (level.Width - 1);
            int imageHeight = 2 + gridSize / 4 + gridSize * (level.Height - 1);
            image = new Bitmap(imageWidth, imageHeight);
            
            DrawGrid(image);
            level.PlankList.ForEach(pla => DrawPlank(image, pla));
            level.SolidList.ForEach(solid => DrawSolid(image, solid));
            picBox.Image = image;

            // Draw the plank image at the top
            picBoxPlank.Image = CreatePlankImage(level.CurPlank);
        }

        /// <summary>
        /// Translates the screen corrdinates to a direction for the player.
        /// </summary>
        /// <param name="posX"></param>
        /// <param name="posY"></param>
        /// <returns></returns>
        public C.DIR GetDirectionFromClick(int posX, int posY)
        {
            int curPosX = offset + level.PlayerPos.X * gridSize;
            int curPosY = offset + level.PlayerPos.Y * gridSize;
            int newPosX = posX - (picBox.Width - image.Width) / 2;
            int newPosY = posY - (picBox.Height - image.Height) / 2;
            int diffX = Math.Abs(curPosX - newPosX);
            int diffY = Math.Abs(curPosY - newPosY);

            if (posX == curPosX && posY == curPosY) return C.DIR.Null;
            else if (diffX < offset && curPosY > newPosY && diffY > diffX) return C.DIR.N;
            else if (diffX < offset && curPosY < newPosY && diffY > diffX) return C.DIR.S;
            else if (diffY < offset && curPosX < newPosX && diffY <= diffX) return C.DIR.E;
            else if (diffY < offset && curPosX > newPosX && diffY <= diffX) return C.DIR.W;
            else return C.DIR.Null;
        }

        /// <summary>
        /// Draws the main level grid.
        /// </summary>
        /// <param name="image"></param>
        private void DrawGrid(Bitmap image)
        {
            for (int x = 0; x < level.Width; x++)
            {
                DrawLine(image, offset + x * gridSize, offset, (level.Height - 1) * gridSize, 1, true, Color.Black);
            }
            for (int y = 0; y < level.Height; y++)
            {
                DrawLine(image, offset, offset + y * gridSize, (level.Width - 1) * gridSize, 1, false, Color.Black);
            }
        }

        /// <summary>
        /// Draws a solid place on the grid.
        /// </summary>
        /// <param name="image"></param>
        /// <param name="square"></param>
        private void DrawSolid(Bitmap image, Point square)
        {
            int radius = gridSize / 8;
            int posX = radius + square.X * gridSize;
            int posY = radius + square.Y * gridSize;

            Color color = Color.White;
            if (square.Equals(level.PlayerPos)) color = Color.Red;
            else if (square.Equals(level.TargetPos)) color = Color.Green;

            DrawCircle(image, posX, posY, radius, Color.Black);
            DrawCircle(image, posX, posY, radius - 2, color);
        }

        /// <summary>
        /// Draws a plank on the image.
        /// </summary>
        /// <param name="image"></param>
        /// <param name="plank"></param>
        private void DrawPlank(Bitmap image, Plank plank)
        {
            // Top left corner of grid square
            int width = Math.Max(level.GridSize / 16, 1);
            int gridPosX;
            int gridPosY;
            if (plank.Direction == C.DIR.S || plank.Direction == C.DIR.E)
            {
                gridPosX = plank.Start.X;
                gridPosY = plank.Start.Y;
            }
            else
            {
                gridPosX = plank.End.X;
                gridPosY = plank.End.Y;
            }

            int PosX = offset + gridPosX * gridSize - width;
            int PosY = offset + gridPosY * gridSize - width;
            int Length = 2 * width + plank.Length * gridSize;
            bool isVertical = plank.Direction.In(C.DIR.S, C.DIR.N);
            // Draw this plank
            DrawLine(image, PosX, PosY, Length, 2 * width + 1, isVertical, Color.Black);
            DrawLine(image, PosX + 1, PosY + 1, Length - 2, 2 * width - 1, isVertical, Color.Brown);
        }

        /// <summary>
        /// Draws the image of a plank, or return null, if the plank was null.
        /// </summary>
        /// <param name="plank"></param>
        /// <returns></returns>
        private Bitmap CreatePlankImage(Plank plank)
        {
            if (plank == null) return null;

            int imageWidth = 2 + plank.Length * gridSize;
            int imageHeight = 2 * Math.Max(gridSize / 16, 1) + 1;
            Bitmap plankImage = new Bitmap(imageWidth, imageHeight);
            DrawRect(plankImage, 0, 0, plankImage.Height, plankImage.Width, Color.Black);
            DrawRect(plankImage, 1, 1, plankImage.Height - 2, plankImage.Width - 2, Color.Brown);
            return plankImage;
        }


        private void DrawLine(Bitmap image, int startX, int startY, int length, int width, bool isVertical, Color color)
        {
            if (isVertical)
            {
                DrawRect(image, startX, startY, length, width, color);
            }
            else
            {
                DrawRect(image, startX, startY, width, length, color);
            }
        }

        private void DrawLine(Bitmap image, int startX, int startY, int length, int width, bool isVertical)
        {
            DrawLine(image, startX, startY, length, width, isVertical, Color.Black);
        }

        private void DrawRect(Bitmap image, int startX, int startY, int height, int width, Color color)
        {
            using (Graphics g = Graphics.FromImage(image))
            {
                using (Brush b = new SolidBrush(color))
                {
                    g.FillRectangle(b, startX, startY, width, height);
                }
            }
        }

        private void DrawCircle(Bitmap image, int centerX, int centerY, int radius, Color color)
        {
            using (Graphics g = Graphics.FromImage(image))
            {
                using (Brush b = new SolidBrush(color))
                {
                    g.FillEllipse(b, centerX - radius, centerY - radius, 2 * radius, 2 * radius);
                }
            }
        }
    }
}
