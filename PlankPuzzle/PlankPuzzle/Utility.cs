﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

namespace PlankPuzzle
{
    class C // for Constants
    {
        public static char DirSep => System.IO.Path.DirectorySeparatorChar;
        public static string NewLine => Environment.NewLine;

        public static string AppPath => Application.StartupPath + DirSep;
        public static string SolutionPath => C.AppPath + "Solutions.txt";

        public enum DIR { Null, N, E, S, W }
        public static readonly DIR[] Dirs = new DIR[] { DIR.N, DIR.E, DIR.S, DIR.W };

        public enum MoveType { Walk, Take, Set }
    }


    static class Utility
    {
        /// <summary>
        /// Checks if an object is contained in an array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool In<T>(this T obj, params T[] args)
        {
            return args.Contains(obj);
        }

        /// <summary>
        /// Parses a string value to an enum of given type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        /// <summary>
        /// Creates a deep clone of a point.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Point Clone(this Point point)
        {
            return new Point(point.X, point.Y);
        }

        /// <summary>
        /// Moves a point in a given direction.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static Point Move(this Point point, C.DIR direction)
        {
            return point.Move(direction, 1);
        }

        /// <summary>
        /// Moves a point in a given direction for a given length.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="direction"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static Point Move(this Point point, C.DIR direction, int length)
        {
            switch (direction)
            {
                case C.DIR.N: point.Y -= length; break;
                case C.DIR.E: point.X += length; break;
                case C.DIR.S: point.Y += length; break;
                case C.DIR.W: point.X -= length; break;
            }
            return point;
        }

        /// <summary>
        /// Returns the opposite direction.
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static C.DIR Opposite(this C.DIR direction)
        {
            switch (direction)
            {
                case C.DIR.N: return C.DIR.S;
                case C.DIR.E: return C.DIR.W;
                case C.DIR.S: return C.DIR.N;
                case C.DIR.W: return C.DIR.E;
                default: return C.DIR.Null;
            }
        }

        /// <summary>
        /// Handles a global unexpected exception and displays a warning message to the user.
        /// </summary>
        /// <param name="Ex"></param>
        public static void HandleGlobalException(Exception Ex)
        {
            try
            {
                LogException(Ex);
                string errorString = "An error occured: " + Ex.Message + C.NewLine + "Try to continue playing? Selecting 'no' will quit the game.";
                var result = MessageBox.Show(errorString, "Error", MessageBoxButtons.YesNo);
                if (result == DialogResult.No) Application.Exit();
            }
            catch
            {
                Application.Exit();
            }
        }

        /// <summary>
        /// Logs an exception message to AppPath/ErrorLog.txt.
        /// </summary>
        /// <param name="ex"></param>
        public static void LogException(Exception ex)
        {
            string errorPath = C.AppPath + "ErrorLog.txt";
            using (System.IO.TextWriter textFile = new System.IO.StreamWriter(errorPath, true))
            {
                textFile.WriteLine(ex.ToString());
            }
        }
    }
}
