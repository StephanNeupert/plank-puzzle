﻿using System;
using System.Windows.Forms;

namespace PlankPuzzle
{
    class MyButton : Button
    {
        public MyButton(int xPos, int yPos, int width, int height, string text)
        {
            Left = xPos;
            Width = width;
            Top = yPos;
            Height = height;
            Text = text;
        }


        protected override bool IsInputKey(Keys keyData)
        {
            return true;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Space)
            {
                base.OnKeyDown(e);
            }
        }
    }
}
