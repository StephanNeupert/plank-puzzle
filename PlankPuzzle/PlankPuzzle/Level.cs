﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace PlankPuzzle
{
    /// <summary>
    /// A plank in the level.
    /// </summary>
    class Plank
    {
        public Point Start;
        public Point End => Start.Clone().Move(Direction, Length);

        public C.DIR Direction;
        public int Length;

        public Plank() { }

        public Plank(Point start, C.DIR dir, int length)
        {
            this.Start = start.Clone();
            this.Direction = dir;
            this.Length = length;
        }

        public Plank Clone()
        {
            return new Plank(this.Start, this.Direction, this.Length);
        }

        private Plank Reverse()
        {
            return new Plank(this.End, this.Direction.Opposite(), this.Length);
        }

        public bool Intersects(Plank otherPlank)
        {
            // First put them in the correct form
            Plank plank1; // vertical
            Plank plank2; // horizontal

            if (this.Direction == otherPlank.Direction || this.Direction == otherPlank.Direction.Opposite())
            {
                return false;
            }
            else if (this.Direction.In(C.DIR.N, C.DIR.S))
            {
                if (this.Direction == C.DIR.S) plank1 = this.Clone();
                else plank1 = this.Reverse();

                if (otherPlank.Direction == C.DIR.E) plank2 = otherPlank.Clone();
                else plank2 = otherPlank.Reverse();
            }
            else
            {
                if (this.Direction == C.DIR.E) plank2 = this.Clone();
                else plank2 = this.Reverse();

                if (otherPlank.Direction == C.DIR.S) plank1 = otherPlank.Clone();
                else plank1 = otherPlank.Reverse();
            }

            return (plank1.Start.Y < plank2.Start.Y 
                 && plank2.Start.Y < plank1.End.Y 
                 && plank2.Start.X < plank1.Start.X
                 && plank1.Start.X < plank2.End.X);
        }
    }

    class Move
    {
        public C.MoveType MoveType; // 0 = walk around, 1 = take plank, 2 = set plank
        public C.DIR Direction;

        public Move(C.MoveType type, C.DIR dir)
        {
            this.MoveType = type;
            this.Direction = dir;
        }

        public Move Clone()
        {
            return new Move(MoveType, Direction);
        }
    }


    class Level
    {
        public Level() { }

        public int Width;
        public int Height;

        public Point PlayerPos;
        public Plank CurPlank;
        public Point TargetPos;

        public List<Plank> PlankList;
        public List<Point> SolidList;
        public List<Move> MoveList;

        public int GridSize => 2 * (2 * 560 / (4 * Math.Max(Width - 1, Height - 1) + 1));

        public Level Clone()
        {
            Level newLevel = new Level();
            newLevel.Width = Width;
            newLevel.Height = Height;
            newLevel.PlayerPos = PlayerPos.Clone();
            newLevel.CurPlank = CurPlank?.Clone();
            newLevel.TargetPos = TargetPos; // no need to clone
            newLevel.PlankList = PlankList.ConvertAll(plank => plank.Clone()).ToList();
            newLevel.SolidList = SolidList.ConvertAll(solid => solid.Clone()).ToList();
            newLevel.MoveList = MoveList.ConvertAll(move => move.Clone()).ToList();

            return newLevel;
        }


        /// <summary>
        /// Takes a plank, if one doesn't carry one.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="doSaveMove"></param>
        /// <returns></returns>
        public bool TakePlank(C.DIR dir, bool doSaveMove = true)
        {
            // May not take more than onw plank
            if (CurPlank != null) return false;

            CurPlank = PlankList.FirstOrDefault(pla => (pla.Start.Equals(PlayerPos) && pla.Direction == dir)
                                                    || (pla.End.Equals(PlayerPos)) && (pla.Direction == dir.Opposite()));

            if (CurPlank == null) return false;

            // Take plank
            PlankList.Remove(CurPlank);
            if (doSaveMove) MoveList.Add(new Move(C.MoveType.Take, dir));
            return true;
        }

        /// <summary>
        /// Sets a plank, if possible.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="doSaveMove"></param>
        /// <returns></returns>
        public bool SetPlank(C.DIR dir, bool doSaveMove = true)
        {
            if (CurPlank == null) return false;

            // Set CurPlank to the correct location                
            CurPlank.Start = PlayerPos.Clone();
            CurPlank.Direction = dir;

            if (MaySetPlank(CurPlank))
            {
                PlankList.Add(CurPlank);
                CurPlank = null;
                if (doSaveMove) MoveList.Add(new Move(C.MoveType.Set, dir));
                return true;
            }
            else
            {
                return false;
            }            
        }

        /// <summary>
        /// Checks whether a plank may be placed at its current location.
        /// </summary>
        /// <param name="plank"></param>
        /// <returns></returns>
        private bool MaySetPlank(Plank plank)
        {
            // Check for solids in the middle
            Point newPos = PlayerPos.Clone();
            for (int i = 1; i < plank.Length; i++)
            {
                newPos = newPos.Move(plank.Direction);
                if (SolidList.Exists(sq => sq.Equals(newPos))) return false;
            }

            // Check for solid at the end
            newPos = newPos.Move(plank.Direction);
            if (!SolidList.Exists(sq => sq.Equals(newPos))) return false;

            // Check for intersection with other planks
            if (PlankList.Exists(pla => pla.Intersects(CurPlank))) return false;

            return true;
        }

        /// <summary>
        /// Moves the player along a plank.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="doSaveMove"></param>
        /// <returns></returns>
        public bool MovePlayer(C.DIR dir, bool doSaveMove = true)
        {
            // Check whether there exists a plank
            Plank plank = PlankList.FirstOrDefault(pla => (pla.Start.Equals(PlayerPos) && pla.Direction == dir)
                                                       || (pla.End.Equals(PlayerPos)) && (pla.Direction == dir.Opposite()));

            if (plank == null) return false;

            // Move to new position
            PlayerPos = PlayerPos.Move(dir, plank.Length);
            if (doSaveMove) MoveList.Add(new Move(C.MoveType.Walk, dir));
            return true;
        }

        /// <summary>
        /// Undos the latest move.
        /// </summary>
        public void UndoMove()
        {
            if (MoveList.Count == 0) return;

            Move lastMove = MoveList[MoveList.Count - 1];
            switch (lastMove.MoveType)
            {
                case C.MoveType.Walk: MovePlayer(lastMove.Direction.Opposite(), false); break;
                case C.MoveType.Take: SetPlank(lastMove.Direction, false); break;
                case C.MoveType.Set: TakePlank(lastMove.Direction, false); break;
            }
            MoveList.Remove(lastMove);
        }

        /// <summary>
        /// Returns whether the level is solved.
        /// </summary>
        /// <returns></returns>
        public bool IsLevelSolved()
        {
            return PlayerPos.Equals(TargetPos);
        }
    }
}
