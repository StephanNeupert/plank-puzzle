﻿namespace PlankPuzzle
{
    partial class FormPlank
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PicBoxLevel = new System.Windows.Forms.PictureBox();
            this.PicBoxPlank = new System.Windows.Forms.PictureBox();
            this.lblLevelTitle = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxPlank)).BeginInit();
            this.SuspendLayout();
            // 
            // PicBoxLevel
            // 
            this.PicBoxLevel.Location = new System.Drawing.Point(10, 60);
            this.PicBoxLevel.Name = "PicBoxLevel";
            this.PicBoxLevel.Size = new System.Drawing.Size(560, 560);
            this.PicBoxLevel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PicBoxLevel.TabIndex = 0;
            this.PicBoxLevel.TabStop = false;
            this.PicBoxLevel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PicBoxLevel_MouseClick);
            // 
            // PicBoxPlank
            // 
            this.PicBoxPlank.Location = new System.Drawing.Point(288, 626);
            this.PicBoxPlank.Name = "PicBoxPlank";
            this.PicBoxPlank.Size = new System.Drawing.Size(282, 39);
            this.PicBoxPlank.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PicBoxPlank.TabIndex = 1;
            this.PicBoxPlank.TabStop = false;
            // 
            // lblLevelTitle
            // 
            this.lblLevelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLevelTitle.Location = new System.Drawing.Point(10, 11);
            this.lblLevelTitle.Name = "lblLevelTitle";
            this.lblLevelTitle.Size = new System.Drawing.Size(560, 34);
            this.lblLevelTitle.TabIndex = 2;
            this.lblLevelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormPlank
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 673);
            this.Controls.Add(this.lblLevelTitle);
            this.Controls.Add(this.PicBoxPlank);
            this.Controls.Add(this.PicBoxLevel);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FormPlank";
            this.Text = "Plank Puzzle";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormMain_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxPlank)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox PicBoxLevel;
        private System.Windows.Forms.PictureBox PicBoxPlank;
        private System.Windows.Forms.Label lblLevelTitle;
    }
}

